The process for this project is as follows:

1) Find the one feature you would like to have
2) Find the smallest step you can take towards having that feature
3) Write that as an Gitlab Issue
4) Work that Issue
5) Check that all other Issues have been completed in all my other projects
6) Return to 2) with current feature or return to 1) with new feature
